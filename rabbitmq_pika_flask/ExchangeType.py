from enum import Enum


class ExchangeType(Enum):
    """The type of exchange to be used
    """

    DEFAULT = 'direct'
    DIRECT = 'direct'
    FANOUT = 'fanout'
    TOPIC = 'topic'
    HEADER = 'header'
